# COS 310 Topics in Computer Science: Intro to Computer Graphics ‒ extra material

Building
========

Clone [CG2 2014 demo](https://github.com/ChaosGroup/cg2_2014_demo) into the parent directory of this repo:

```
$ cd ..
$ git clone https://github.com/ChaosGroup/cg2_2014_demo
```

Invoke the build script from this repo:

```
$ cd -
$ ./build.sh
```

Build artifacts are in `problem_4.app/Contents/MacOS`. Invoke executable as:

```
$ cd problem_4.app/Contents/MacOS
$ ./problem_4
```

Control Macros
--------------

Following macros, found in Makefile, control various parameters of the build:

* FB_RES_FIXED_W ‒ fixed framebuffer horizontal resolution
* FB_RES_FIXED_H ‒ fixed framebuffer vertical resolution; combined with the above both override screen geometry control; speeds up some calculations by using literals instead of variables
* AO_NUM_RAYS ‒ ambient-occlusion rays per pixel
